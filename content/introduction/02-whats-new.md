+++
title = "What's new?"
weight = 2
+++

These summary reports document progress milestones and highlight accomplished features.

## October 7th, 2022

This week we added `ansible-lint` to the Ansible SSA execution environment. This makes it easier to use the EE in Visual Studio Code to get syntax highlighting and tab completion with the collections inside our EE.

Also we finally managed to upgrade to Red Hat Ansible Automation Platform 2.2 - which automatically retires our support for 2.1.

## July 1st, 2022

It's been a long time since this section has been updated. It doesn't mean we were not working - on the contrary, a lot has happened.

- fully switched to Red Hat Ansible Automation Platform 2

- reorganized the CI pipeline completely, including building Execution Environments

- bring back releases for each component for stable, reliable and predictable deployments

- new roles e.g. to register a system on RHSM or Red Hat Insights

- so much more...

We are planning to updated this document more regularly in the future. Promise.

## March 12th, 2021

We constantly try to make it easier to consume the content provided in this project. We started to experiment with [Red Hat CodeReady Workspaces](https://www.redhat.com/en/technologies/jboss-middleware/codeready-workspaces) and can now provide instructions to [automatically setup your workstation](../../installation/10-setup-workstation/#automatic-setup).

The easiest option is to sign up for a free [Red Hat Developer account](https://developers.redhat.com/) and use the [Developer Sandbox](https://developers.redhat.com/developer-sandbox).

## February 26th, 2021

To be ready for the upcoming "Ansible Execution Environments" and Best Practices, we created an [ansible_ssa.common](https://gitlab.com/ansible-ssa/ansible-ssa-collection) collection which combines all relevant roles in this project in one collection. It was also a good exercise to get familiar with `git submodule` and to build a CI/CD pipeline which builds a new collection automatically.

## February 12th, 2021

We were able to continue our work around private Automation Hub. Since last week, the playbooks can be used to (optionally) deploy a second cloud instance as an private Automation Hub. Since some [variables were renamed](../../installation/30-extra-variables/), we bumped the version to 2.0.0 to indicate that we break backwards compatibility.

## February 5th, 2021

We've not updated this page for a while - but that doesn't mean we were lazy! Welcome to 2021 and check out all the news!

Last week we set up a new server which acts as our DynDNS Server and wrote some Playbooks to set it up and manage it. Now we can offer a subdomain like &lt;username&gt;.ansible-labs.de which is fully controlled by the respective owner.

e.g. if you install automation you will have tower.&lt;mysuffix&gt;.ansible-labs.de automatically managed and deleted by the Ansible "instance" role.

Details and instructions on how to request access can be found in the chapter on [extra variables](../../installation/30-extra-variables/#dynamic-dns)

After this was done, we continued work of a unified "[tower-install.yml](../../installation/40-rhaap-install/)" which calls tower-instance, tower-setup and tower-content in the proper order, reducing provisioning time and lowering the entry barrier. Note this will only work if you have the DynDNS magic enabled.

Last but not least we started working on upgrading to Tower 3.8 and adding (optionally) an private Automation Hub next to your tower - so stay tuned for next week, when it's hopefully working.

## September 11th, 2020

Finally we had time to document the Tomcat use cases! Check out the new chapters on [RBAC](../../usage/02-rbac/) and [Tomcat](../../usage/03-tomcat/).

A lot of effort was put into creating a CI/CD pipeline to run automated tests when merging code into the "master" branch. The pipeline will create a test VM, install and configure Ansible Tower, setup the content and finally delete it again. Although the initial work has been done, there is a lot more to do and we're still far away from having full test coverage.

The [IDM](https://gitlab.com/ansible-ssa/playbook-idm) use case has also seen some additional development. It should now also work on Azure where it failed before.

## September 4th, 2020

The use cases to add Red Hat Identity Manager has seen some additional improvements, but is still missing documentation. The Playbooks to deploy and manage Tomcat have also seen some minor fixes, which haven't been documented either. Clearly, we're bit behind on documentation...

We introduced a new ["Newbie" label](https://gitlab.com/groups/ansible-ssa/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Newbie) to make it easier to get started and contribute to this projects. Issues which have a low risk of breaking stuff and which are considered to be easy to implement, are labeled with it.

## August 28th, 2020

The new [tomcat use case](https://gitlab.com/groups/ansible-ssa/-/milestones/4) saw some additional improvements on working with different users, roles and organizations. After some more polishing and QE, we still need to better document how to use the new preconfigured content in a demo scenario.

We also added [Red Hat Identity Manager](https://gitlab.com/ansible-ssa/playbook-idm3) to the [Tower Job Templates](https://gitlab.com/ansible-ssa/role-tower-content/-/issues/5). This will deploy and configure an RH IDM Server for you. We will later build a use case around this to demonstrate RH IDM as our single sign on platform for all demo systems, including Ansible Tower itself and Red Hat Satellite.

## August 21st, 2020

To be able to demonstrate RBAC we created three organizations:

- Engineering
- Marketing
- Sales

Each organization has two users developer and operator, which have also been created in the default organization. What's missing is documentation about the complete list of users, groups, roles and permissions.

We also added optional support for Red Hat Ansible Automation Hub to automatically download and install certified content. The first role utilizing this content is [role-tomcat-ops](https://gitlab.com/ansible-ssa/playbook-tomcat-ops). It's using the ansible.posix collection to make sure SElinux is in enforcing mode. Although this is a tiny example, more will follow soon, for example we are planning to [switch to the Red Hat supported Collection](https://gitlab.com/ansible-ssa/playbook-satellite/-/issues/3) to manage Red Hat Satellite.

## July 31st, 2020

This week we finished the LAMP Workflow use case. The new [AWX Collection](https://galaxy.ansible.com/awx/awx) makes it very easy to build [Workflows in Ansible Tower](https://gitlab.com/ansible-ssa/role-tower-content/-/blob/master/tasks/use-case-workflow.yml).

## July 24th, 2020

This was a quiet week which saw some improvements on Deploying and Retiring Windows Server instances on GCP. To better track the different activities, a [Add Windows Support](https://gitlab.com/groups/ansible-ssa/-/milestones/3) milestone has been created.

Work on adding an Ansible Tower Workflow to deploy a [multi tier LAMP stack](https://gitlab.com/ansible-ssa/role-tower-content/-/issues/37)] has seen some significant [number of commits](https://gitlab.com/ansible-ssa/role-tower-content/-/commits/37-make-the-lamp-workflow-a-proper-use-case).

## July 3rd, 2020

Notable fixes and improvements:

- some of the the Foremen modules were renamed and broke the Satellite Use Case ([satellite-content #2](https://gitlab.com/ansible-ssa/role-satellite-content/-/issues/2), [satellite-setup #4](https://gitlab.com/ansible-ssa/role-satellite-setup/-/issues/4))

- packages from EPEL break the Satellite installer, however EPEL is enabled by default on some cloud providers ([satellite-setup #6](https://gitlab.com/ansible-ssa/role-satellite-setup/-/issues/6))

- more boolean fixes (use [boolean filter](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html) instead of true/false)

- the [Satellite Use Case](../../installation/50-use-case-satellite/) is done for Google Cloud and Azure

If you find a bug you want to propose an idea for a new feature, please use the [GitLab Issue Tracker](https://gitlab.com/groups/ansible-ssa/-/issues)

[Open Feature Requests](https://gitlab.com/groups/ansible-ssa/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Feature%20Request)

[Open Bugs](https://gitlab.com/groups/ansible-ssa/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Bug)
