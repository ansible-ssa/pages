+++
title = "Tools and Git Flow"
weight = 10
+++

To contribute to this project we have best practices, rules and guidelines to follow.

## License

We aim to publish all code under the terms of the GPL in public repositories. Sometimes we want to keep things internal and mark repositories as "private", but this the exception and not the norm.

## Tools

We're using GitLab to manage our source code, write documentation and track progress.

### GitLab Issue Tracker

All work is tracked in the [GitLab Issue Tracker](https://gitlab.com/groups/ansible-ssa/-/issues). Use the repository specific issues if a problem or feature is directly related to a repository. If it's not possible to have such a direct relationship, create the issue in the parent project. When start working on an issue, assign it to yourself so others can see that someone is working on it.

Merge Requests can only be created if they reference an existing issue. Merge Requests should link back to the issue by using GitLab feature to [close issues automatically](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically). Don't forget to use the [Work in Progress](https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html) flag to let others know things are not completely done.

Use labels to make it easier to find issues and filter for issue categories:

- Feature Request: Propose an idea to be added (use color code \#69D100)

- Bug: Something doesn't work and needs fixing (use color code \#FF0000)

- Documentation: update or add something to the documentation in the [pages](https://gitlab.com/ansible-ssa/pages) project (use color code \##6699cc)

- Newbie: easy to implement and low risk of breaking something (use color code \#D1D100)

The labels are not pre-populated on all projects and if missing will be created on their first use.

Projects can use additional labels when needed.

### Git Flow

We're using a variant of [Git Flow](https://nvie.com/posts/a-successful-git-branching-model/) when developing content for this project.

'main' is always the latest development version. We never break 'main'! Development is done in a separate branch and merged into 'main' after passing all tests.

The release branch is forked from 'main' and contains the latest stable release. Releases are tagged. We follow [Semantic Versioning](https://semver.org/) for assigning release tags. We do not have a release schedule yet, new releases are created when we see fit.

New features are branched from 'main' into feature branches. When a feature is complete it is merged back into main by using a Merge Request. Merge Requests can only be opened from an issue. While work on a Merge Request is not completed, it should have an [WIP or draft flag](https://docs.gitlab.com/ee/user/project/merge_requests/drafts.html).

#### Protected branches

To make sure we do not accidentally release broken code, the 'main' and 'release' branches are protected branches.

Only maintainers can push to and merge protected branches, Code Owner approval is not required.

#### Incubation phase

If you start a new role it might sound like overkill to have the full process and restrictions explained above, in place. In the early stages of a new project, the rules can be skipped. A repository leaves incubation phase when it is used by other roles or playbooks in their respective 'main' or 'release' branch.

The maturity matrix describes the status of each repository (ordered by maturity level and alphabetically):

| Repository Path | Description | Maturity Level | Pipeline status |
|---|---|---|---|
| [playbook-rhaap](https://gitlab.com/ansible-ssa/playbook-rhaap/) | This playbook can be used to setup an ansible automation platform | stable | Main: [![Pipeline Status Main Branch](https://gitlab.com/ansible-ssa/playbook-rhaap/badges/main/pipeline.svg?classes=inline)](https://gitlab.com/ansible-ssa/playbook-rhaap/-/pipelines), Release: [![Pipeline Status Release Branch](https://gitlab.com/ansible-ssa/playbook-rhaap/badges/release/pipeline.svg?classes=inline)](https://gitlab.com/ansible-ssa/playbook-rhaap/-/pipelines) |
| [ansible-ssa-collection](https://gitlab.com/ansible-ssa/ansible-ssa-collection) | Ansible Collection which provides all the roles in one package | stable | Main: [![Pipeline Status Main Branch](https://gitlab.com/ansible-ssa/ansible-ssa-collection/badges/main/pipeline.svg?classes=inline)](https://gitlab.com/ansible-ssa/ansible-ssa-collection/-/pipelines), Release: [![Pipeline Status Release Branch](https://gitlab.com/ansible-ssa/ansible-ssa-collection/badges/release/pipeline.svg?classes=inline)](https://gitlab.com/ansible-ssa/ansible-ssa-collection/-/pipelines) |
| [role-automationhub-content](https://gitlab.com/ansible-ssa/role-automationhub-content/) | This role will configure automation hub remote repositories, collection sync and execution environments | stable | Main: [![Pipeline Status Main Branch](https://gitlab.com/ansible-ssa/role-automationhub-content/badges/main/pipeline.svg?classes=inline)](https://gitlab.com/ansible-ssa/role-automationhub-content/-/pipelines) |
| [role-automationhub-setup](https://gitlab.com/ansible-ssa/role-automationhub-setup/) | this role can be used to install the necessary packages and install automation hub | stable | Main: [![Pipeline Status Main Branch](https://gitlab.com/ansible-ssa/role-automationhub-setup/badges/main/pipeline.svg?classes=inline)](https://gitlab.com/ansible-ssa/role-automationhub-setup/-/pipelines) |
| [role-controller-content](https://gitlab.com/ansible-ssa/role-controller-content/) | This role will setup job templates, inventories, credentials and more | stable | Main: [![Pipeline Status Main Branch](https://gitlab.com/ansible-ssa/role-automationhub-content/badges/main/pipeline.svg?classes=inline)](https://gitlab.com/ansible-ssa/role-automationhub-content/-/pipelines) |
| [role-controller-setup](https://gitlab.com/ansible-ssa/role-controller-setup/) | this role can be used to install the necessary packages and install automation controller | stable | Main: [![Pipeline Status Main Branch](https://gitlab.com/ansible-ssa/role-automationhub-setup/badges/main/pipeline.svg?classes=inline)](https://gitlab.com/ansible-ssa/role-automationhub-setup/-/pipelines) |
| [role-epel](https://gitlab.com/ansible-ssa/role-epel/) | setup EPEL repositories for the detected operating system | stable | Main: [![Pipeline Status Main Branch](https://gitlab.com/ansible-ssa/role-epel/badges/main/pipeline.svg?classes=inline)](https://gitlab.com/ansible-ssa/role-epel/-/pipelines), Release: [![Pipeline Status Release Branch](https://gitlab.com/ansible-ssa/role-epel/badges/release/pipeline.svg?classes=inline)](https://gitlab.com/ansible-ssa/role-epel/-/pipelines) |
| [role-instance](https://gitlab.com/ansible-ssa/role-instance/) | this will role can be used to create and remove instances on several providers | stable | Main: [![Pipeline Status Main Branch](https://gitlab.com/ansible-ssa/role-instance/badges/main/pipeline.svg?classes=inline)](https://gitlab.com/ansible-ssa/role-epel/-/pipelines), Release: [![Pipeline Status Release Branch](https://gitlab.com/ansible-ssa/role-instance/badges/release/pipeline.svg?classes=inline)](https://gitlab.com/ansible-ssa/role-epel/-/pipelines) |
| [playbook-workflow-lamp](https://gitlab.com/ansible-ssa/playbook-workflow-lamp/) | provides the content for the LAMP workflow use case | stable | |
| [playbook-satellite](https://gitlab.com/ansible-ssa/playbook-satellite/) | This will use the role-satellite-content and role-satellite-setup to install and setup a Satellite Server including some demo content | incubation | |
| [role-satellite-content](https://gitlab.com/ansible-ssa/role-satellite-content/) | This role will configure Satellite 6 content like repositories, content views etc. | incubation | |
| [role-satellite-setup](https://gitlab.com/ansible-ssa/role-satellite-setup/) | This role will perform the installation of Satellite 6 and the minimum setup | incubation | |
| [role-satellite-register](https://gitlab.com/ansible-ssa/role-satellite-register/) | This is a role helps to register rhel machines to Red Hat Satellite 6.x and subscribe it to the needed repositories | incubation | |
| [role-simple-lamp](https://gitlab.com/ansible-ssa/role-simple-lamp/) | This is a tiny lamp project | incubation | |
| [role-vmware-instance](https://gitlab.com/ansible-ssa/role-vmware-instance/) | This role helps to provision a virtual machine in VMware with a given spec. | incubation | |
| [role-vcenter-vm-basic](https://gitlab.com/ansible-ssa/role-vcenter-vm-basic/) | missing | incubation | |
| [pages](https://gitlab.com/ansible-ssa/pages/) | Project Documentation | stable | Main: [![Pipeline Status Main Branch](https://gitlab.com/ansible-ssa/pages/badges/main/pipeline.svg?classes=inline)](https://gitlab.com/ansible-ssa/pages/-/pipelines) |

{{% notice note %}}
This page does not automatically reload, reload manually to get the updated CI/CD pipeline badges.
{{% /notice %}}

## Documentation

All documentation is done in Markdown in the [pages repository](https://gitlab.com/ansible-ssa/pages). Unlike other code repositories, we do not follow the Git Flow approach for documentation. The 'main' branch always contains the latest published version of the documentation. However, we still create branches and Merge Requests to track larger or complex changes.

The documentation is published by using [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) and built automatically from a CI/CD pipeline using [Hugo](https://gohugo.io/) and the [learn theme](https://gohugo.io/).

### Target audience

THere are several different areas of documentation which is written for different target audiences:

- User Guide: This is not a "How to learn Ansible" course, so we assume you know the basics to run Ansible Playbooks, the concepts of Red Hat Ansible Automation Platform and how to use it. We do however explain more advanced features of Ansible or automation controller or relatively new concepts and approaches. We assume you know Linux, Windows, Networking, how to write simple Ansible Playbooks and the automation controller User Interface.

- Developer Guide: This is written for experienced Ansible and Git users. We assume you know your way around those tools and will not describe any basics. We assume you can write complex Ansible Playbooks, Roles, Collections, can interact with automation controller by using modules, the CLI or the REST API. We also assume you know git, know how to write nice playbooks, are familiar with the Ansible best practices, ansible-lint, etc.
