+++
title = "Structure"
weight = 10
+++

If there is a README.md in the project, the pipeline will automatically generate a chapter here. Every time a project is changed, these pages are updated. Documentation is always built from the `main` branch.
