+++
title = "Introduction"
weight = 10
+++

If you didn't follow the installation guide, follow it's [instructions](../../installation/) first.

Each of the following chapters will describe how to different capabilities to customers. The individual demo scenarios can be combined together to build a comprehensive user story.

Some demos will require specific settings when setting up the demo environment. Since the installation is done by using Ansible, you can change configuration settings and enable additional use cases after the initial setup. Although the provided Playbooks have been written to modular and extensible, scaling down is typically not supported. For example, if a use case was enabled by setting the respective variable, setting it back to false and disabling the use case will not reverse the previous changes. Keeping that in mind, you should only enable use cases you are planning to use in your demo.

Some of the optional use cases will deploy additional VMs or instances making the full demo more expensive. To reduce cost, you should be able to stop and restart instances when needed.
