+++
title = "Example"
weight = 20
+++

## Overview

This is an example of how use cases should be documented. Make a copy of this file, rename it to `50-my-use-case.md` and update its content.

Every use case should start with a short description. This should also include information about why this is relevant to customers, or which problem we're addressing.

## Prerequisites

In this chapter we document the technical requirements to demo this use case. This typically includes `extra_vars` which have to be configured, but can also include other requirements like a specific public cloud provider.

## Caveats

Add any potential challenges or issues in this chapter. For example certain tasks take a long time to complete, known issues or limitations.

## How to use

This should be a detailed list of what has to be done to demo this use case. Try to find a balance of not going to detailed, but still providing all relevant information to successfully complete the demo. This could include things like which buttons have to be clicked in the automation controller UI, or emphasis certain important points of the demo.

Keep in mind the target audience to demo the use case is SA/SSA level with at least intermediate Ansible and AAP knowledge.
