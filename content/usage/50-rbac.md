+++
title = "RBAC"
weight = 50
+++

## Overview

The [Role Based Access Control](https://docs.ansible.com/automation-controller/latest/html/userguide/organizations.html) (RBAC) features of automation controller allow administrators to delegate and grant privileges to organizations, teams and users. The following instructions can be used to demonstrate the RBAC model.

The automation controller provided by this demo is already configured with the following identities:

| Organizations |
|---|
| Default |
| Engineering |
| Sales |
| Marketing |

Each organization comes with a "developer" and an "operator" user. Their passwords are the same as the automation controller admin password.

The following privileges are configured in the "Default" organization:

| Identity | Object | Permissions |
|---|---|---|
| Developer | Job Template "Development - Deploy WebApp", Job Template "Development - Install and Setup Tomcat" | admin - developer user can manage and launch this job template |
| Developer | Project "GitLab playbook-tomcat-dev" | admin - developer can manage this Git Repository |
| Developer | Job Template "Operations - Open Firewall Port" | execute - developer can launch this job template, but the job template definition is read only |
| Operator | Project "GitLab playbook-tomcat-ops" | admin - operator can manage this Git Repository |
| Operator | Job Template "Development - Install and Setup Tomcat", "Operations - Deploy new Virtual Machine", "Operations - Base configuration", "Operations - Tomcat Compliance Check" | admin - operator can manage and launch them |
| Operator | Job Template "Operations - Open Firewall Port" | admin - operator can manage and launch this job template |
| Developer, Operator | Inventory | use - operator and developer can use the inventory created during provisioning |

**NOTE:** The "Operations - Open Firewall Port" Job template currently only works on Google Cloud and is not created with any other provider.

Read the section on the [Tomcat](../03-tomcat/) use case to learn more about the job templates and what they do.

## Prerequisites

Make sure to enable the [Tomcat Use Case](../../installation/55-use-case-tomcat/).

## Caveats

Considerations before running the demo:

- a new instance is deployed which makes your demo more expensive

- keep in mind deploying the instance will take will take a minute or two, make sure you have something to talk about while the instance is created

- don't forget to [delete the instance](../../installation/99-decommission/) when you're done

- there will be no DNS record for the Tomcat instance

## How to use

It is recommended to get familiar with the different users and their respective permissions. They have been designed with the following workflow in mind:

- log in as operator

  - demonstrate the fact that many areas in the UI are empty, or list only very few items

  - highlight we do not see any credentials, but we can consume them in our playbooks

  - deploy a new instance - "Operations - Deploy new Virtual Machine"

  - apply base configuration to the new instance - "Operations - Base configuration"

- log in as developer

  - give a quick tour and show the changes in job templates, projects, inventories, ...

  - highlight we do not see any credentials, but we can consume them in our playbooks

  - install Tomcat - "Development - Install and Setup Tomcat"

  - deploy the example WebApp - "Development - Deploy WebApp"

  - open a new browser and show that VM is not accessible (why?)

  - open firewall rule to make VM accessible - "Operations - Open Firewall Port" - ONLY WORKS ON GCP!

  - connect to the example app: http://<IP or FQDN>:8080/demo-webapp/

- log in as admin

  - show all the new details like many more job temples, credentials, inventories, ...

  - show we can see all job outputs from all previous jobs

  - run the compliance check - "Operations - Tomcat Compliance Check"

  - show the job output - it should show a difference in directory permissions

  - change the job type from "check" to "run" to automatically fix the mismatch - with this we demonstrate that we can show mismatch, but even better: we can automatically fix it with the same playbook (no code changes!)

  - run it again to show idempotency and desired state configuration
