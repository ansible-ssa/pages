+++
title = "Red Hat Insights"
weight = 50
+++

Detailed instructions on how to demo Red Hat Insights with this project can be found on [GitHub](https://github.com/amayagil/deploy_machines/blob/main/roles/role-insights-registration/Insights_AAP2_Integration.md).
