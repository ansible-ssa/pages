+++
title = "All-in-one RHAAP Installation"
weight = 40
+++

You can create the automation controller and automation hub instances, install Red Hat Ansible Automation Platform and configure it in one single playbook run. Just use the `rhaap-install.yml` playbook and it will take care of the entire provisioning process. If you stored your variables in a file called `variables/main.yml` and your Ansible Vault in `variables/vault.yml`, you can use the following command:

```bash
# if you're a Red Hat employee, log into Quay.io to be able to download the latest version of the execution environment
podman login quay.io
# run the playbook with ansible-navigator
ansible-navigator run rhaap-install.yml -e @variables/main.yml -e @variables/vault.yml
```

There is a default `ansible-navigator.yml` configuration file which will download the latest version of the Ansible SSA execution environment.

{{% notice warning %}}
Make sure to configure the variables and environment for your provider, before running the playbook.
{{% /notice %}}
