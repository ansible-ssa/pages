+++
title = "Use Case: Windows Server"
weight = 51
+++

{{% notice warning %}}
This use case is currently in development for Google Cloud and has not been adopted to other cloud providers yet.
{{% /notice %}}

To enable this use case make sure to set the following variable:

```yaml
use_case_windows: true
```

### Deploy a new Windows instance

Run the job template `Windows - Deploy a new instance`.
