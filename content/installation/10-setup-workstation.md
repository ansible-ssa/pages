+++
title = "Setup workstation"
weight = 10
+++

## Installation and Configuration

To be able to use the content in this repository, a few requirements on your workstation have to be met:

- install ansible-navigator

- choose a provider and setup credentials: right now the environment can be setup on Azure, Amazon AWS, GCP or RHV

- populate the extra variables

You can install `ansible-navigator` from the Red Hat Ansible Automation Platform repository on RHEL, or via pip on Fedora. If you use Fedora and want to install it via pip, we recommend to use a dedicated virtual environment.

```bash
virtualenv ansible-navigator
source ansible-navigator/bin/activate
pip install ansible-navigator
```

## What to deploy

The idea of these demos and use cases is to be modular as much as possible. This means you can deploy only the demos and use cases you want to use which will help you to save cost (particularly if you plan to use public cloud), but also keep the provisioning time to the necessary minimum.

However, all demos are build to run on top of an automation controller. You can use an existing automation controller, deploy one yourself or let the playbooks do it for you. Depending on this decision, you will have to provide different environment variables and run different playbooks.

## Check out the main repository

To provision an Red Hat Ansible Automation Platform, configure it and setup the content, you will have to clone the [playbook-rhaap](http://gitlab.com/ansible-ssa/playbook-rhaap/) repository:

```bash
# checkout release branch of the project
git clone -b release http://gitlab.com/ansible-ssa/playbook-rhaap.git
# if you already checked out the repository before, make your your local version is up to date
git checkout release
git pull
```

{{% notice note %}}
The release branch will give you the latest stable release. Releases are also tagged to be able to easily roll back, if needed. Read the [Contribute](../../contribute/) section to learn more on our branches and how to collaborate on new features.
{{% /notice %}}

This repository is using a number of roles packaged in one Ansible Collection. We provide an execution environment which contains everything you'll need. Every time `ansible-navigator` is started, it will check for a newer stable release of the execution environment.

## SSH Key ring

To be able to log into the managed nodes, Ansible uses SSH by default. Since the private key has to be imported into the automation controller, you might want to use a dedicated key pair. Although the keys are stored encrypted in the PostgreSQL database, there is a risk someone might log into the automation controller and retrieve the SSH private key - particularly if you use a weak password for the automation controller.

{{% notice warning %}}
Always use strong passwords! Never upload your clear text passwords into git repositories, in particular not into GitLab or GitHub public projects. It is also therefore highly recommended to create a new SSH key ring dedicated to the usage with this demo project.
{{% /notice %}}

Create a new key pair with the following command:

```bash
ssh-keygen -f ansible_labs
```

Add these keys into the respective variables and store them outside of your git repository, or use `ansible-vault` to encrypt them.

## SSH Agent

Ansible is using SSH to run commands on the managed node and `ansible-navigator` of course is no different. Since the playbook is now running in an execution environment, the best option is to use an [ssh-agent](https://man.openbsd.org/ssh-agent) to forward your SSH key into the execution environment.

Most Linux Desktops start an SSH Agent for your and make it easy to use. If you're new to SSH agents and want to do things manually on the command line, here are the necessary steps.

```bash
eval `ssh-agent -s`
ssh-add /path/to/your/keyring
```

Afterwards run `ansible-navigator` in the same shell. [KDE](https://wiki.archlinux.org/title/KDE_Wallet) and [GNOME](https://wiki.gnome.org/Apps/Seahorse/SSHAgent) have their built-in SSH agents which my require different steps.
