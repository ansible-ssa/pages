+++
title = "Decommission"
weight = 99
+++

After you're done, you should delete your deployment. This can be done by calling the playbook with variable `remove` set to `true`.

```bash
ansible-navigator run rhaap-install.yml -e @variables/main.yml -e @variables/vault.yml -e remove=true
```

If you created additional instances during the demo, you can use this playbook to also remove them one by one:

```bash
# replace my-instance wit the name of the instance to be deleted
ansible-navigator run rhaap-install.yml -e @variables/main.yml -e @variables/vault.yml -e remove=true -e instance_name=my-instance
```
