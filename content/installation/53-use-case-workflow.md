+++
title = "Use Case: LAMP Workflow"
weight = 53
+++

{{% notice warning %}}
This use case is currently in development for Google Cloud and has not been adopted to other cloud providers yet.
This use case is currently deployed on one single virtual machine. Later, It will be adjusted scale out.
{{% /notice %}}

To enable this use case make sure to set the following variable:

```yaml
use_case_workflow: true
```

## About LAMP Workflow

This is use case deploys a virtual instance, then configures it to run a web application (index.php) using both PHP/MariaDB to demonstrate the power of Ansible to orchestrate the complete process.

### Start the LAMP Workflow

Run the job template `lamp-workflow`. Then keep monitoring all the nodes until all steps successfully completed.

### Test the web application

As soon as the process of workflow is completed. Open the browser on the ip of the newly created machine called `webapp`.

http://ip-address/

You should see the message : Connected successfully !!

Then the list of databases below.
