# README

These are the markdown sources for the project documentation. For better viewing experience, checkout [https://redhat-cop.gitlab.io/ansible-ssa/pages](https://redhat-cop.gitlab.io/ansible-ssa/pages).

## GitLab repository documentation

`git submodule` is used to populated the content of the folder `content/contribute/projects`. To add new repositories to the documentation, use the following command:

```bash
# this is just an example, role-instance was already added, use the name of a missing or new repository instead
repository=role-instance
git submodule add -b release --name $repository -- ../$repository.git content/contribute/projects/$repository
git commit -m "adding new sub module"
git push
```

**NOTE:** Make sure to not add private repositories by accident!
